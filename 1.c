#include <stdio.h>
#include <stdlib.h>

/* Initialize the program */
int main()
{
	/*           *
	 * Variables *
	 * 	     */
	
	char textLetter = 'A';
	/* Character */

	char textName[] = "NANASHI";
	/* String, Array */

	int textAge = 18;
	/* Interger */

	double textMoney = 20.25;
	/* Decimal (Double) */
	
	const int CONSTANT_ONE = 1;
	const char CONSTANT_TWO[] = "Two";
	/* Declare constants with const.
	 * Constants can't be modified later in the code.
	 * Use caps to distinguish them from normal vars. */


	
	/*	  *
	 * printf *
	 * 	  */

	printf("Hi there.\n");
	/* Print text lol */

	printf("My name is %s, what's yours?\n", textName);
	/* Print variable strings */

	printf("My last name starts with %c.\n", textLetter);
	/* Print variable single letter */

	printf("I am %d years old.\n", textAge);
	/* Print variable intergers */

	printf("The amount of money I have in my wallet is $%f.\n", textMoney);
	/* Print variable decimal/float */

	printf("Testing: %s %d %f\n", "HUNDRED", 50 + 50, 100.0)
	/* You don't always need to assign a variable to printf.
	 * You can also do math within the printf parameters. */
	
	
	
	/*	*
	 * Math *
	 * 	*/

	printf("Ten to the second power is %d.\n", pow(10, 2));
	/* pow is for exponentials.
	 * The second number is the power. */
	
	printf("The square root of 100 is %d.\n", sqrt(100));
	/* sqrt is for square roots, obviously. */

	printf("99.9 rounded up is %d.\n", ceil(99.9));
	/* ceil, or ceiling, rounds up. */

	printf("100.1 rounded down is %d.\n", floor(100.1));
	/* floor does the opposite of ceil. */

	
	
	/*	      *
	 * User Input *
	 * 	      */

	int inputInt;
	printf("This prompt is asking for user input.\nPlease enter a number: ";
	scanf("%d", &inputInt); /* Use &var to assign variable to input. */
	printf("Confirmation line: Your input was the number %d.\n", inputInt);
	
	/* Note that when scanf'ing for a float, use %lf instead of %f. 
	 * You would still use %f for the printf output. */

	char inputChar;
	printf("Input a character: ");
	scanf("%c", &inputChar);
	printf("Your letter is: %c\n", inputChar);

	char inputString[20]; /* Number is the limit of how many characters can be stored. */
	printf("Enter your name: ");
	scanf("%s", inputString); /* No need for & for strings. */
	printf(" Your name is %s.\n", inputString);
	/* IMPORTANT! scanf only grabs characters in a string up to the first space.
	 * Use fgets instead, shown below. */

	char inputFgets[64];
        printf("Enter your full name: ");
        fgets(inputFgets, 64, stdin); /* stdin = Standard Input */
        printf(" Your name is %s.\n", inputFgets);
	/* An issue with fgets is that it stores ENTER/RETURN input
	 * in the string as well, which results in the string containing
	 * a new line which may lead to problems. */

	return 0;
}
