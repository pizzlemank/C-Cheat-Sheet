#include <stdio.h>
#include <stdlib.h>

double cube(double num) {
	double result = num * num * num;
	return result;

	/* Alternatively,
	 * return num * num * num; */
}

int main(){

	printf("Answer: %f", cube(3.0));

	return 0;
}

/* Function must be above main() if main() refers to a variable made by the function!
 * i.e. see how cube is used within the printf statement? It calls for it, and
 * if the cube function is below it then the main function doesn't know that it exists.
 *
 * However, if you need main() to be above the cube function for some reason,
 * you can do the following:
 *
 *
 * double cube(double num);
 *
 * int main(){
 * ~~ code ~~
 * }
 *
 * double cube (double num){
 * ~~ code ~~
 * }
 *
 *
 * This makes sure that cube is referred to properly :) */
