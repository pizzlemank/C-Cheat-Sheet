#include <stdio.h>
#include <stdlib.h>

int main()
{
	int luckyNumbers[7] /*# of values in array */ = {1, 7, 9, 10, 11, 17, 19};
	luckyNumbers[3] = 0; /* Assign int within array */
	printf("%d\n", luckyNumbers[3]); /* Starts at 0 */
	sayHi("lol707"); /* Stores lol707 inside name array in sayHi */
	return 0;
}

void sayHi(char name[]){
/* void - doesn't return any information */
	printf("\n\nsayHi function says:\nHello user %s!\n\n", name);

}
